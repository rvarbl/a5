# README #

Kasutatud allikad internetist:
Abi / ideed rekursiivseks stringi Node'deks parsimisel:
https://github.com/kkorvel/home5/blob/master/src/Node.java
https://enos.itcollege.ee/~japoia/algoritmid/puud.html
Queue kasutamine:
https://docs.oracle.com/javase/7/docs/api/java/util/Queue.html
StringTokenizer:
https://docs.oracle.com/javase/7/docs/api/java/util/StringTokenizer.html
https://moodle.taltech.ee/mod/forum/discuss.php?d=105216

## Command line examples. Näidete kasutamine käsurealt ##

#### Compilation. Kompileerimine: ####

```
#!bash

javac -cp src src/Node.java
```

#### Execution. Käivitamine: ####

```
#!bash

java -cp src Node
```

### Usage of tests. Testide kasutamine ###

#### Compilation of a test. Testi kompileerimine: ####

```
#!bash

javac -encoding utf8 -cp 'src:test:test/junit-4.13.1.jar:test/hamcrest-core-1.3.jar' test/NodeTest.java

```

In Windows replace colons by semicolons. Sama Windows aknas (koolonite asemel peavad olema semikoolonid):

```
#!bash

javac -encoding utf8 -cp 'src;test;test/junit-4.13.1.jar;test/hamcrest-core-1.3.jar' test/NodeTest.java


```

#### Running a test. Testi käivitamine: ####

```
#!bash

java -cp 'src:test:test/junit-4.13.1.jar:test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore NodeTest
```

The same for Windows. Sama Windows aknas (koolonite asemel semikoolonid):

```
#!bash

java -cp 'src;test;test/junit-4.13.1.jar;test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore NodeTest
```
