
import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    public boolean hasChildren() {
        return this.firstChild != null;
    }

    public boolean hasSiblings() {
        return this.nextSibling != null;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Node parsePostfix(String s) {
        Queue<String> tokens;
        try {
            tokens = parseString(s);
        } catch (RuntimeException e) {
            throw new RuntimeException("Invalid input \"" + s + "\"\n Error: " + e.getMessage());
        }

        Node node = new Node(null, null, null);
        while (!tokens.isEmpty()) {
            String token = tokens.remove();
            if (token.equals("(")) {
                try {
                    node.firstChild = parseChildren(tokens);
                    if (node.firstChild.name.equals("")) {
                        throw new RuntimeException("Invalid input \"" + s
                                + "\"\n Nameless node was created, check commas and brackets");
                    }
                } catch (RuntimeException e) {
                    throw new RuntimeException("Invalid input \"" + s + "\"\n Error: " + e.getMessage());
                }
            } else if (token.equals(",")) {
                try {
                    node.nextSibling = parseChildren(tokens);
                    if (node.name == null) {
                        throw new RuntimeException("Invalid input \"" + s
                                + "\"\n Nameless node was created, check commas and brackets");
                    }
                } catch (RuntimeException e) {
                    throw new RuntimeException("Invalid input \"" + s + "\"\n Error: " + e.getMessage());
                }
            } else if (token.equals(")")) {
                break;
            } else {
                node.name = token;
            }
        }
        if (node.name == null) {
            throw new RuntimeException(("Invalid input \"" + s
                    + "\"\n Nameless node was created, check commas and brackets"));
        }
        if (!node.hasChildren() && node.hasSiblings()) {
            throw new RuntimeException("Invalid input \"" + s + "\"\n Error: Missing brackets");
        }
        if (node.hasSiblings()) {
            throw new RuntimeException("Invalid input \"" + s + "\"\n Error: Root node can not have siblings");
        }
        return node;
    }

    private static Node parseChildren(Queue<String> strings) {
        Node node = new Node(null, null, null);
        String name = "";
        while (!strings.isEmpty()) {
            String token = strings.remove();
            if (token.equals("(")) {
                node.firstChild = parseChildren(strings);
            } else if (token.equals(",")) {
                node.nextSibling = parseChildren(strings);
                node.name = name;
                return node;
            } else if (token.equals(")")) {
                node.name = name;
                return node;
            } else {
                name = token;
            }
        }
        return node;
    }

    private static Queue<String> parseString(String s) throws RuntimeException {
        if (s.contains(",,")) {
            throw new RuntimeException("Input contains empty sibling node! " +
                    "\n (two commas next to each other)");
        }
        if (s.contains("()")) {
            throw new RuntimeException("Input contains empty child node!" +
                    "\n (empty brackets in string)");
        }
        Queue<String> strings = new LinkedList<>();

        StringTokenizer tokens = new StringTokenizer(s, "(,)", true);
        while (tokens.hasMoreTokens()) {
            String token = tokens.nextToken();
            if (token == null || token.replaceAll("[ \t\n]", "").equals("")) {
                throw new RuntimeException("Input contains empty element");
            }
            token = token.trim();
            if (token.contains(" ")) {
                throw new RuntimeException("Element \"" + token + "\" contains whitespaces");
            }
            strings.add(token);
        }
        return strings;
    }


    public String leftParentheticRepresentation() {
        StringBuilder builder = new StringBuilder().append(name);

        if (hasChildren()) {
            builder.append(getChildren(firstChild));
        }
        return builder.toString();
    }

    private String getChildren(Node node) {
        StringBuilder builder = new StringBuilder();
        builder.append("(").append(node.name);
        if (node.hasChildren()) {
            builder.append(getChildren(node.firstChild));
        }
        Node sibling = node.nextSibling;
        while (sibling != null) {
            builder.append(", ").append(sibling.name);
            if (sibling.hasChildren()) {
                builder.append(getChildren(sibling.firstChild));
            }
            sibling = sibling.nextSibling;
        }

        builder.append(")");

        return builder.toString();
    }


    public static void main(String[] param) {
//        String s = "%%";            // ok
//         String s = "((@,#)+)-34";   // ok
//        String s = "((@, #)+)-34";  // ok
//         String s = "     \t ";      // not ok
//         String s = "((1),(2)3)4";   // not ok
//         String s = "A B";           // not ok
        String s = "((3 , 5)6)7";  // not ok
        System.out.println("Input: " + s);
        Node tree = Node.parsePostfix(s);
        System.out.println(" Left: "
                + tree.leftParentheticRepresentation());
    }

}

